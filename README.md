# Reverse Proxy for VMs

This is a reverse proxy using HAProxy to enable multiple VMs share the same IPv4
and IPv6. At the moment, it only tunnels TCP on port 443 (HTTPS), based on TLS
SNI.

Deployment happens via Ansible.

## Configuration

A proxied VM still requires a dedicated IPv6. From that, the hostname and the IP
of the host the service is hosted on have to be entered in the host_vars of the
proxy on the cluster the VM is located on.

Example, e.g. in `host_vars/proxy.plutex.fsfeurope.org`, proxy the hostname
`proxytest1.fsfe.org` to its IPv6:

```yml
webservers:
  proxytest1.fsfe.org: 2a02:16d0:1004:5a00:f5f3::1
```

## VM webserver configuration

The webservers behind this reverse proxy have to be slightly re-configured in
order to work properly. That is because we use the proxy protocol to forward
requests, and the real originating IP should be displayed correctly in the logs.

### Apache

1. Enable the required module: `a2enmod remoteip`
2. In the virtual host config (e.g. `/etc/apache2/sites-enabled/000-default.conf`), add inside the vhost of port 443:

```
RemoteIPProxyProtocol On
RemoteIPTrustedProxy proxy.plutex.fsfeurope.org
RemoteIPTrustedProxy proxy.noris.fsfeurope.org
```

3. In the Apache config, modify the default log format to show the real IP:

```diff
- LogFormat "%h %l %u %t \"%r\" %>s %O \"%{Referer}i\" \"%{User-Agent}i\"" combined
+ LogFormat "%a %l %u %t \"%r\" %>s %O \"%{Referer}i\" \"%{User-Agent}i\"" combined
```

### Nginx

Nginx has the required module included in Debian's default installation.

1. Modify the following to the virtual host configuration (e.g. `/etc/nginx/sites-enabled/default`), again the port 443 config:

```diff
-listen [::]:443 ssl ipv6only;
-listen 443 ssl;
+listen [::]:443 ssl ipv6only proxy_protocol;
+listen 443 ssl proxy_protocol;
```

2. Add in the same configuration:

```
set_real_ip_from proxy.plutex.fsfeurope.org;
set_real_ip_from proxy.noris.fsfeurope.org;
```
